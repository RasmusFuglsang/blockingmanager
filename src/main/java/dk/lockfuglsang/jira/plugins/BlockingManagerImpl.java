package dk.lockfuglsang.jira.plugins;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.config.StatusManager;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.issue.status.category.StatusCategory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.workflow.IssueWorkflowManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.Arrays;
import java.util.Collection;

public class BlockingManagerImpl implements InitializingBean, DisposableBean {
  private static final Collection<Long> ISSUE_EDITED = Arrays.asList(EventType.ISSUE_COMMENTED_ID, EventType.ISSUE_UPDATED_ID, EventType.ISSUE_CREATED_ID);
  private static final Collection<Long> ISSUE_RESOLVED = Arrays.asList(EventType.ISSUE_RESOLVED_ID, EventType.ISSUE_CLOSED_ID);

  private final ApplicationProperties applicationProperties;
  private final EventPublisher eventPublisher;
  private final StatusManager statusManager;
  private final IssueLinkManager issueLinkManager;
  private final IssueService issueService;
  private final JiraAuthenticationContext authenticationContext;
  private final IssueWorkflowManager workflowManager;

  public BlockingManagerImpl(ApplicationProperties applicationProperties, EventPublisher eventPublisher,
                             StatusManager statusManager, IssueLinkManager issueLinkManager, IssueService issueService,
                             JiraAuthenticationContext authenticationContext, IssueWorkflowManager workflowManager) {
    this.applicationProperties = applicationProperties;
    this.eventPublisher = eventPublisher;
    this.statusManager = statusManager;
    this.issueLinkManager = issueLinkManager;
    this.issueService = issueService;
    this.authenticationContext = authenticationContext;
    this.workflowManager = workflowManager;
  }

  @EventListener
  public void onIssueEvent(IssueEvent event) {
    Long eventTypeId = event.getEventTypeId();
    if (isIssueUpdated(eventTypeId)) {
      checkBlockedLinks(event.getIssue());
    }
    if (isIssueResolved(eventTypeId)) {
      checkBlockedIssues(event.getIssue());
    }
  }

  private void checkBlockedLinks(Issue issue) {
    Status currentStatus = issue.getStatusObject();
    if (!currentStatus.getStatusCategory().getKey().equalsIgnoreCase("done")) {
      for (IssueLink link : issueLinkManager.getOutwardLinks(issue.getId())) {
        if (isBlocksLink(link)) {
          if (!isStatusBlocked(link.getDestinationObject())) {
            // TODO: rlf: 15 Aug 2014 - Update destination -> BLOCKED
            updateStatusToBlocked(link.getDestinationObject());
          }
        }
      }
    }
  }

  private void updateStatusToBlocked(Issue destinationObject) {
    User user = authenticationContext.getUser().getDirectoryUser();
    IssueInputParameters parameters = issueService.newIssueInputParameters();
    // TODO: rlf: 15 Aug 2014 - Transition instead of just updating...
    ActionDescriptor action = getBlockedAction(destinationObject);
    if (action != null) {
      IssueService.TransitionValidationResult result = issueService.validateTransition(user, destinationObject.getId(), action.getId(), parameters);
      if (result.isValid()) {
        issueService.transition(user, result);
      } else {
        // TODO: rlf: 18 Aug 2014 - Figure out how to show these errors
      }
    } else {
      // TODO: rlf: 18 Aug 2014 - Figure out what happens here?
    }
  }

  private ActionDescriptor getBlockedAction(Issue issue) {
    Collection<ActionDescriptor> availableActions = workflowManager.getAvailableActions(issue, authenticationContext.getUser());
    for (ActionDescriptor descriptor : availableActions) {
      if (descriptor.getName().equalsIgnoreCase("blocked")) {
        return descriptor;
      }
    }
    return null;
  }

  private ActionDescriptor getOpenAction(Issue issue) {
    Collection<ActionDescriptor> availableActions = workflowManager.getAvailableActions(issue, authenticationContext.getUser());
    for (ActionDescriptor descriptor : availableActions) {
      if (descriptor.getName().equalsIgnoreCase("unblock")) {
        return descriptor;
      }
    }
    return null;
  }

  private void checkBlockedIssues(Issue issue) {
    // TODO: rlf: 15 Aug 2014 - If any issues where blocked by this one and in the BLOCKED state, transition them
    for (IssueLink link : issueLinkManager.getOutwardLinks(issue.getId())) {
      if (isBlocksLink(link)) {
        Issue destIssue = link.getDestinationObject();
        if (isStatusBlocked(destIssue) && !hasOtherBlockers(destIssue, issue)) {
          // TODO: rlf: 15 Aug 2014 - Update destination -> OPEN
          updateStatusToOpen(destIssue);
        }
      }
    }
  }

  private boolean isBlocksLink(IssueLink link) {
    return link.getIssueLinkType().getName().equalsIgnoreCase("blocks");
  }

  /**
   * Whether the destIssue is blocked by any other issue that <code>issue</code>
   */
  private boolean hasOtherBlockers(Issue destIssue, Issue issue) {
    for (IssueLink link : issueLinkManager.getInwardLinks(destIssue.getId())) {
      if (isBlocksLink(link) && !link.getSourceId().equals(issue.getId()) && !isDone(link.getSourceObject())) {
        return true;
      }
    }
    return false;
  }

  private boolean isDone(Issue issue) {
    return issue.getStatusObject().getStatusCategory().getKey().equals(StatusCategory.COMPLETE);
  }

  private boolean isStatusBlocked(Issue destIssue) {
    return destIssue.getStatusObject().getName().equalsIgnoreCase("blocked");
  }

  private void updateStatusToOpen(Issue destinationObject) {
    User user = authenticationContext.getUser().getDirectoryUser();
    IssueInputParameters parameters = issueService.newIssueInputParameters();
    // TODO: rlf: 15 Aug 2014 - Transition instead of just updating...
    ActionDescriptor action = getOpenAction(destinationObject);
    if (action != null) {
      IssueService.TransitionValidationResult result = issueService.validateTransition(user, destinationObject.getId(), action.getId(), parameters);
      if (result.isValid()) {
        issueService.transition(user, result);
      } else {
        // TODO: rlf: 18 Aug 2014 - Figure out how to show these errors
      }
    } else {
      // TODO: rlf: 18 Aug 2014 - Figure out what happens here?
    }
  }

  private boolean isIssueResolved(Long eventTypeId) {
    return ISSUE_RESOLVED.contains(eventTypeId);
  }

  private boolean isIssueUpdated(Long eventTypeId) {
    return ISSUE_EDITED.contains(eventTypeId);
  }

  @Override
  public void destroy() throws Exception {
    eventPublisher.unregister(this);
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    eventPublisher.register(this);
  }
}